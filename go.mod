module codeberg.org/gruf/gibon

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-bytesize v0.2.0
	codeberg.org/gruf/go-env v1.1.3
	codeberg.org/gruf/go-format v1.0.6
	codeberg.org/gruf/go-kv v1.2.1
	codeberg.org/gruf/go-logger/v2 v2.0.4
	codeberg.org/gruf/go-maxbytes v1.0.5
	codeberg.org/gruf/go-middleware v1.1.0
	codeberg.org/gruf/go-mimetypes v1.0.0
	codeberg.org/gruf/go-pools v1.1.0
	codeberg.org/gruf/go-store v1.3.8
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4
)
