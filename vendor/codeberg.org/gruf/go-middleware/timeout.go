package middleware

import (
	"context"
	"net/http"
	"sync"
	"time"

	"codeberg.org/gruf/go-ctx"
)

// Timeout adds user defined request timeouts to provided handler, either by "Request-Timeout" header or the given
// fallback value. Please note that this handler also provides logging so do not wrap within Middleware.Logger().
func (m *Middleware) Timeout(handler http.Handler, fallback time.Duration) http.Handler {
	ctxPool := sync.Pool{New: func() interface{} { return ctx.New() }}
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// Wrap the ResponseWriter
		trw := &TimeoutResponseWriter{rw: rw}

		// Check request for provided timeout
		timeout := r.Header.Get("Request-Timeout")

		// Attempt to parse a timeout value from raw
		dur, _ := time.ParseDuration(timeout)
		if dur < 1 {
			dur = fallback
		}

		// Prepare the timeout ctx
		trw.ctx = ctxPool.Get().(*ctx.Context)
		trw.cncl = trw.ctx.WithCancel()

		// Replace request context
		rctx := r.Context()
		done := trw.ctx.Done() // our own ptr
		r = r.WithContext(&doneContext{
			// we prefer our timeout channel
			// to the originating context
			Context: rctx,
			done:    done,
		})

		go func() {
			// We run the handler in a separate goroutine
			// so as not to block the main handler thread,
			// allowing a timeout error to be sent and the
			// connection closed without worrying about
			// blocking behaviour within the provided handler.

			// Start request timer
			before := time.Now()
			trw.ctx.WithDeadline(before.Add(dur))

			defer func() {
				// Stop the handler timer
				diff := time.Since(before)

				// Ensure cancelled
				cancelled := trw.Cancel()

				if cancelled /* i.e. NOT timeout */ {
					m.log.LogRequest(r, trw.Status(), diff)
				}

				// Remove ctx
				ctx := trw.ctx
				trw.ctx = nil

				// Place in pool
				ctx.Reset()
				ctxPool.Put(ctx)
			}()

			// Pass to HTTP handler
			handler.ServeHTTP(trw, r)
		}()

		// Wait on context
		// timeout / cancel
		<-done

		// Attempt to mark ourselves as timed out (will fail if the
		// status has already been set). On success, send response + log
		if cas(&trw.vals, 0, 0, http.StatusRequestTimeout, 1) {
			m.HTTPError(rw, r, "Request Timed Out", http.StatusRequestTimeout, http.ErrHandlerTimeout)
		}
	})
}

// doneContext wraps a context.Context to override it's
// original done channel with our own provided done channel.
type doneContext struct {
	context.Context
	done <-chan struct{}
}

func (ctx *doneContext) Done() <-chan struct{} {
	return ctx.done
}

// TimeoutResponseWriter wraps an http.ResponseWriter to add request timeout capabilities.
type TimeoutResponseWriter struct {
	ctx  *ctx.Context        // ctx is the context we use for timeout
	rw   http.ResponseWriter // underlying wrapped http.ResponseWriter
	cncl func()              // cncl is the context cancel function
	vals uint64              // vals is an atomic store of status code and timeout status
}

// Status returns the set response status code.
func (rw *TimeoutResponseWriter) Status() int {
	code, _ := load(&rw.vals)
	return int(code)
}

// Header implements http.ResponseWriter's .Header().
func (rw *TimeoutResponseWriter) Header() http.Header {
	return rw.rw.Header()
}

// WriteHeader implements http.ResponseWriter's .WriteHeader().
func (rw *TimeoutResponseWriter) WriteHeader(status int) {
	// Load timeout status
	_, tout := load(&rw.vals)

	// Write status code if not already set
	if cas(&rw.vals, 0, tout, uint32(status), tout) {
		rw.rw.WriteHeader(status)
	}
}

// Write implements http.ResponseWriter's .Write().
func (rw *TimeoutResponseWriter) Write(b []byte) (int, error) {
	// Load timeout status
	_, tout := load(&rw.vals)

	// Check if timed out
	if tout == 1 {
		return 0, http.ErrHandlerTimeout
	}

	// Write default status code if none is set
	if cas(&rw.vals, 0, tout, 200, tout) {
		rw.rw.WriteHeader(200)
	}

	return rw.rw.Write(b)
}

// ResponseWriter returns access to the underlying http.ResponseWriter.
func (rw *TimeoutResponseWriter) ResponseWriter() http.ResponseWriter {
	return rw.rw
}

// TimedOut returns whether this TimeoutResponseWriter has timed out.
func (rw *TimeoutResponseWriter) TimedOut() bool {
	_, tout := load(&rw.vals)
	return tout == 1
}

// Cancel will attempt to cancel the timeout counter.
func (rw *TimeoutResponseWriter) Cancel() bool {
	if code, tout := load(&rw.vals); tout == 0 &&
		// attempt to mark the tout state as cancelled
		cas(&rw.vals, code, 0, code, 2) {
		rw.cncl() // cancel the context
		return true
	}
	return false
}
