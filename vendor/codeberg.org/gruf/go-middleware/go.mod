module codeberg.org/gruf/go-middleware

go 1.17

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-ctx v1.0.2
	codeberg.org/gruf/go-kv v1.2.0
	codeberg.org/gruf/go-ulid v1.0.0
	github.com/felixge/httpsnoop v1.0.2
)

require codeberg.org/gruf/go-buf v1.0.0
