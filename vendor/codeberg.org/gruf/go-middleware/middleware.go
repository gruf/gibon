package middleware

import (
	"log"
	"net/http"
	"runtime"
	"strconv"
	"time"

	"codeberg.org/gruf/go-buf"
	"codeberg.org/gruf/go-kv"
)

type Middleware struct{ log Logger }

// New returns a new instance of Middleware, using log.Default() is Logger is nil.
func New(logger Logger) *Middleware {
	if logger == nil {
		logger = (*StdLogger)(log.Default())
	}
	return &Middleware{logger}
}

// Logger is a log interface used by middleware in its
// various handlers. This allows your log implementation
// of choice to work with our middleware handlers.
type Logger interface {
	// LogRequest will log a handled HTTP request along with the handler's response code and latency.
	LogRequest(r *http.Request, code int, latency time.Duration)

	// LogHTTP will log the error response of a handled HTTP request including status, code and the causal error.
	LogHTTPError(r *http.Request, status string, code int, err error)

	// LogPanic will log a panic caught when handling an HTTP request including recovered panic info and stack frames.
	LogPanic(r *http.Request, i interface{}, stack *runtime.Frames)
}

// StdLogger is a typedef to allow the standard library "log"
// Logger to implement the middleware.Logger interface.
type StdLogger log.Logger

func (l *StdLogger) LogRequest(r *http.Request, code int, latency time.Duration) {
	fields := make([]kv.Field, 0, 6)

	// If request ID set, append this
	if id, ok := GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: string(idKey), V: id,
		})
	}

	// Append the request info fields
	fields = append(fields, []kv.Field{
		{K: "method", V: r.Method},
		{K: "uri", V: r.URL.RequestURI()},
		{K: "code", V: code},
		{K: "latency", V: latency},
		{K: "msg", V: "http request"},
	}...)

	// Output these entry fields to log
	_ = (*log.Logger)(l).Output(3, kv.Fields(fields).String())
}

func (l *StdLogger) LogHTTPError(r *http.Request, status string, code int, err error) {
	fields := make([]kv.Field, 0, 5)

	// If request ID set, append this
	if id, ok := GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: string(idKey), V: id,
		})
	}

	// Append error response fields
	fields = append(fields, []kv.Field{
		{K: "status", V: status},
		{K: "code", V: code},
		{K: "error", V: err},
		{K: "msg", V: "http error"},
	}...)

	// Output these entry fields to log
	_ = (*log.Logger)(l).Output(3, kv.Fields(fields).String())
}

func (l *StdLogger) LogPanic(r *http.Request, i interface{}, stack *runtime.Frames) {
	var buf buf.Buffer
	fields := make([]kv.Field, 0, 4)

	for {
		// Look for next frame
		f, ok := stack.Next()
		if !ok {
			break
		}

		// Append stack frame to buffer
		buf.WriteString(f.File)
		buf.WriteString(" #")
		buf.B = strconv.AppendInt(buf.B, 10, f.Line)
		buf.WriteString(": ")
		buf.WriteString(f.Function)
		buf.WriteByte('\n')
	}

	// If request ID set, append this
	if id, ok := GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: string(idKey), V: id,
		})
	}

	// Append panic info fields
	fields = append(fields, []kv.Field{
		{K: "panic", V: i},
		{K: "stacktrace", V: buf.String()},
		{K: "msg", V: "caught panic"},
	}...)

	// Output these entry fields to log
	_ = (*log.Logger)(l).Output(3, kv.Fields(fields).String())
}
