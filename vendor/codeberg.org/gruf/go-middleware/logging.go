package middleware

import (
	"context"
	"net/http"
	"runtime"
	"time"

	"codeberg.org/gruf/go-ulid"
)

// ctxkey is our unique ctx key type.
type ctxkey string

// idKey is the ctx key for request IDs.
const idKey = ctxkey("id")

// GetRequestID fetches the unique request ID from a request.
func GetRequestID(r *http.Request) (string, bool) {
	id, ok := r.Context().Value(idKey).(string)
	return id, ok
}

// RequestID provides setting unique IDs per request and adding a hook to the logger to append
// these to log entries using the request's context. This should be placed at the TOP of handler stack.
func (m *Middleware) RequestID(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// Generate new ULID
		ulid, err := ulid.New()
		if err == nil {
			// Attach ULID str to context
			ctx := context.WithValue(
				r.Context(), idKey,
				ulid.String(),
			)

			// Replace req context
			r = r.WithContext(ctx)
		}

		// Pass to HTTP handler
		handler.ServeHTTP(rw, r)
	})
}

// Logger adds request logging to the provided HTTP handler.
func (m *Middleware) Logger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		var status *int

		// We need to wrap 'rw' to snoop status code
		rw, status = snoopStatusCode(rw)

		// Get before-request time
		before := time.Now()

		defer func() {
			// Stop timer now done
			diff := time.Since(before)

			// If status is 0, set default
			if *status == 0 {
				*status = 200
			}

			// Log this request
			m.log.LogRequest(r, *status, diff)
		}()

		// Perform handler
		handler.ServeHTTP(rw, r)
	})
}

// Recovery adds panic recovery (and panic logging) to the provided HTTP handler.
func (m *Middleware) Recovery(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		defer func() {
			if i := recover(); i != nil {
				// Attempt to send error response, this may be useless
				http.Error(rw, "Internal Server Error", http.StatusInternalServerError)

				// Prepare a stacktrace
				pcs := make([]uintptr, 10)
				depth := runtime.Callers(2, pcs)
				frames := runtime.CallersFrames(pcs[:depth])

				// Log this panic
				m.log.LogPanic(r, i, frames)
			}
		}()

		// Pass to HTTP handler
		handler.ServeHTTP(rw, r)
	})
}

// HTTPError handles responding to a client with an HTTP error code and logs the provided error.
func (m *Middleware) HTTPError(rw http.ResponseWriter, r *http.Request, msg string, code int, err error) {
	// Respond to client with error
	http.Error(rw, msg, code)

	// Post the error log entry
	m.log.LogHTTPError(r, msg, code, err)
}
