package ulid

import (
	"reflect"
	"unsafe"
)

// s2b converts string to bytes with no alloc (well, minimal alloc).
func s2b(s string) []byte {
	var b []byte

	// Get byte + string headers
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))

	// Manually set bytes to string
	bh.Data = sh.Data
	bh.Len = sh.Len
	bh.Cap = sh.Len

	return b
}
