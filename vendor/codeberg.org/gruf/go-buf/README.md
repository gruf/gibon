# go-buf

This library provides a simple, fast, byte buffer with access to the underlying byte slice.