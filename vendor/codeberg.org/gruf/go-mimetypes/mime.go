package mimetypes

import "path"

// GetForFilename returns mimetype for given filename.
func GetForFilename(filename string) (string, bool) {
	ext := path.Ext(filename)
	if len(ext) < 1 {
		return "", false
	}
	return GetForExt(ext[1:])
}

// GetForExt returns mimetype for given file extension.
func GetForExt(ext string) (string, bool) {
	mime, ok := mimeTypes[ext]
	return mime, ok
}
