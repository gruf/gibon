package httplog

import (
	"net/http"
	"runtime"
	"strconv"
	"time"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v2"
	"codeberg.org/gruf/go-logger/v2/level"
	"codeberg.org/gruf/go-middleware"
)

// ensure Logger conforms to interface.
var _ middleware.Logger = (*Logger)(nil)

// Logger is a typedef for logger.Logger to allow it to
// implement the middleware.Logger interface -- allowing
// use in the middleware package.
type Logger logger.Logger

func (l *Logger) LogRequest(r *http.Request, code int, latency time.Duration) {
	fields := make([]kv.Field, 0, 6)

	// If request ID set, append this
	if id, ok := middleware.GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: "id", V: id,
		})
	}

	// Append the request info fields
	fields = append(fields, []kv.Field{
		{K: "method", V: r.Method},
		{K: "uri", V: r.URL.Path},
		{K: "code", V: code},
		{K: "latency", V: latency},
		{K: "msg", V: "http request"},
	}...)

	// Output these entry fields to log
	(*logger.Logger)(l).LogKVs(3, level.INFO, fields...)
}

func (l *Logger) LogHTTPError(r *http.Request, status string, code int, err error) {
	fields := make([]kv.Field, 0, 5)

	// If request ID set, append this
	if id, ok := middleware.GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: "id", V: id,
		})
	}

	// Append error response fields
	fields = append(fields, []kv.Field{
		{K: "status", V: status},
		{K: "code", V: code},
		{K: "error", V: err},
		{K: "msg", V: "http error"},
	}...)

	// Output these entry fields to log
	(*logger.Logger)(l).LogKVs(3, level.ERROR, fields...)
}

func (l *Logger) LogPanic(r *http.Request, i interface{}, stack *runtime.Frames) {
	var buf byteutil.Buffer
	fields := make([]kv.Field, 0, 4)

	for {
		// Look for next frame
		f, ok := stack.Next()
		if !ok {
			break
		}

		// Append stack frame to buffer
		buf.WriteString(f.File)
		buf.WriteString(` #`)
		buf.B = strconv.AppendInt(buf.B, int64(f.Line), 10)
		buf.WriteString(`: `)
		buf.WriteString(f.Function)
		buf.WriteByte('\n')
	}

	// If request ID set, append this
	if id, ok := middleware.GetRequestID(r); ok {
		fields = append(fields, kv.Field{
			K: "id", V: id,
		})
	}

	// Append panic info fields
	fields = append(fields, []kv.Field{
		{K: "panic", V: i},
		{K: "stacktrace", V: buf.String()},
		{K: "msg", V: "caught panic"},
	}...)

	// Output these entry fields to log
	(*logger.Logger)(l).LogKVs(3, level.ERROR, fields...)
}
