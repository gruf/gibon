module codeberg.org/gruf/go-bitutil

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-errors/v2 v2.0.0
)
