package maxbytes

import (
	"io"
)

// Reader provides a limited io.Reader.
type Reader struct {
	r io.Reader
	n int64
}

// NewReader returns a new maxbytes.Reader wrapping supplied io.Reader.
func NewReader(r io.Reader, limit int64) io.Reader {
	return &Reader{r, limit}
}

func (r *Reader) Read(b []byte) (int, error) {
	if r.n < 1 {
		return 0, io.EOF
	}

	if int64(len(b)) > r.n {
		b = b[:r.n]
	}

	n, err := r.r.Read(b)
	r.n -= int64(n)

	return n, err
}

// Writer provides a limited io.Writer.
type Writer struct {
	w io.Writer
	n int64
}

// NewWriter returns a new maxbytes.Writer wrapping supplied io.Writer
func NewWriter(w io.Writer, limit int64) io.Writer {
	return &Writer{w, limit}
}

func (w *Writer) Write(b []byte) (int, error) {
	if w.n < 1 {
		return 0, io.EOF
	}

	if int64(len(b)) > w.n {
		b = b[:w.n]
	}

	n, err := w.w.Write(b)
	w.n -= int64(n)

	return n, err
}
