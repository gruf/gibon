module codeberg.org/gruf/go-env

go 1.17

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-bytesize v0.1.0
)
