package env

import (
	"strconv"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-bytesize"
)

func Set(key string, value string) {
	if err := syscall.Setenv(key, value); err != nil {
		panic("invalid env key")
	}
}

func SetBool(key string, value bool) {
	Set(key, strconv.FormatBool(value))
}

func SetInt(key string, value int) {
	Set(key, strconv.Itoa(value))
}

func SetInt64(key string, value int64) {
	Set(key, strconv.FormatInt(value, 10))
}

func SetUint(key string, value uint) {
	Set(key, strconv.FormatUint(uint64(value), 10))
}

func SetUint64(key string, value uint64) {
	Set(key, strconv.FormatUint(value, 10))
}

func SetFloat(key string, value float64) {
	Set(key, strconv.FormatFloat(value, 'f', -1, 64))
}

func SetDuration(key string, value time.Duration) {
	Set(key, value.String())
}

func SetTime(key string, value time.Time) {
	Set(key, value.Format(time.RFC1123))
}

func SetSize(key string, value bytesize.Size) {
	Set(key, value.StringIEC())
}

func SetBytes(key string, value []byte) {
	Set(key, string(value))
}

func SetArray(key string, value []string) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteByte('"')
		buf.WriteString(strings.ReplaceAll(value[i], "\"", "\\\""))
		buf.WriteByte('"')
	}, len(value))
}

func SetBoolArray(key string, value []bool) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.FormatBool(value[i]))
	}, len(value))
}

func SetIntArray(key string, value []int) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.Itoa(value[i]))
	}, len(value))
}

func SetInt64Array(key string, value []int64) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.FormatInt(value[i], 10))
	}, len(value))
}

func SetUintArray(key string, value []uint) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.FormatUint(uint64(value[i]), 10))
	}, len(value))
}

func SetUint64Array(key string, value []uint64) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.FormatUint(value[i], 10))
	}, len(value))
}

func SetFloatArray(key string, value []float64) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(strconv.FormatFloat(value[i], 'f', -1, 64))
	}, len(value))
}

func SetDurationArray(key string, value []time.Duration) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(value[i].String())
	}, len(value))
}

func SetTimeArray(key string, value []time.Time) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteByte('"')
		buf.WriteString(value[i].Format(time.RFC1123))
		buf.WriteByte('"')
	}, len(value))
}

func SetSizeArray(key string, value []bytesize.Size) {
	SetEnvArray(key, func(buf *bytes.Buffer, i int) {
		buf.WriteString(value[i].StringIEC())
	}, len(value))
}

func SetEnvArray(key string, append func(*bytes.Buffer, int), len int) {
	buf := bytes.Buffer{}

	buf.WriteByte('[')

	for i := 0; i < len; i++ {
		append(&buf, i)
		buf.WriteString(", ")
	}

	if len > 0 {
		buf.Truncate(2)
	}

	buf.WriteByte(']')

	Set(key, buf.StringPtr())
}
