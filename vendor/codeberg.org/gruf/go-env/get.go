package env

import (
	"strconv"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-env/util"
)

func Get(key string) string {
	str, _ := syscall.Getenv(key)
	return str
}

func GetBool(key string) bool {
	str, ok := syscall.Getenv(key)
	if !ok {
		return false
	}
	val, err := strconv.ParseBool(str)
	if err != nil {
		panic("env: invalid bool value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetInt(key string) int {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := strconv.Atoi(str)
	if err != nil {
		panic("env: invalid int value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetInt64(key string) int64 {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		panic("env: invalid int64 value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetUint(key string) uint {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := strconv.ParseUint(str, 10, strconv.IntSize)
	if err != nil {
		panic("env: invalid uint value for key '" + key + "': " + err.Error())
	}
	return uint(val)
}

func GetUint64(key string) uint64 {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := strconv.ParseUint(str, 10, 64)
	if err != nil {
		panic("env: invalid uint64 value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetFloat(key string) float64 {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := strconv.ParseFloat(str, 64)
	if err != nil {
		panic("env: invalid float value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetDuration(key string) time.Duration {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := time.ParseDuration(str)
	if err != nil {
		panic("env: invalid time.Duration value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetTime(key string) time.Time {
	str, ok := syscall.Getenv(key)
	if !ok {
		return time.Time{}
	}
	val, err := time.Parse(time.RFC1123, str)
	if err != nil {
		panic("env: invalid time.Time value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetSize(key string) bytesize.Size {
	str, ok := syscall.Getenv(key)
	if !ok {
		return 0
	}
	val, err := bytesize.ParseSize(str)
	if err != nil {
		panic("env: invalid bytesize.Size value for key '" + key + "': " + err.Error())
	}
	return val
}

func GetBytes(key string) []byte {
	str, _ := syscall.Getenv(key)
	return bytes.StringToBytes(str)
}

func GetArray(key string) []string {
	var vals []string

	ok := GetEnvArray(key, func(str string) error {
		vals = append(vals, str)
		return nil
	})

	if ok && len(vals) < 1 {
		vals = []string{}
	}

	return vals
}

func GetBoolArray(key string) []bool {
	var vals []bool

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.ParseBool(str)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []bool{}
	}

	return vals
}

func GetIntArray(key string) []int {
	var vals []int

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.Atoi(str)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []int{}
	}

	return vals
}

func GetInt64Array(key string) []int64 {
	var vals []int64

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.ParseInt(str, 10, 64)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []int64{}
	}

	return vals
}

func GetUintArray(key string) []uint {
	var vals []uint

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.ParseUint(str, 10, 64)
		if err == nil {
			vals = append(vals, uint(val))
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []uint{}
	}

	return vals
}

func GetUint64Array(key string) []uint64 {
	var vals []uint64

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.ParseUint(str, 10, 64)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []uint64{}
	}

	return vals
}

func GetFloatArray(key string) []float64 {
	var vals []float64

	ok := GetEnvArray(key, func(str string) error {
		val, err := strconv.ParseFloat(str, 64)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []float64{}
	}

	return vals
}

func GetDurationArray(key string) []time.Duration {
	var vals []time.Duration

	ok := GetEnvArray(key, func(str string) error {
		val, err := time.ParseDuration(str)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []time.Duration{}
	}

	return vals
}

func GetTimeArray(key string) []time.Time {
	var vals []time.Time

	ok := GetEnvArray(key, func(str string) error {
		val, err := time.Parse(time.RFC1123, str)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []time.Time{}
	}

	return vals
}

func GetSizeArray(key string) []bytesize.Size {
	var vals []bytesize.Size

	ok := GetEnvArray(key, func(str string) error {
		val, err := bytesize.ParseSize(str)
		if err == nil {
			vals = append(vals, val)
		}
		return err
	})

	if ok && len(vals) < 1 {
		vals = []bytesize.Size{}
	}

	return vals
}

// GetEnvArray gets env var and splits a slice formatted string i.e. `["hello world", key2 key3, "\"quoted\" key"]`.
func GetEnvArray(key string, each func(string) error) bool {
	str, ok := syscall.Getenv(key)
	if !ok {
		return false
	}

	if !strings.HasPrefix(str, "[") {
		panic("env: invalid slice value for key '" + key + "': missing slice open '['")
	}

	str = str[1:]

	if !strings.HasSuffix(str, "]") {
		panic("env: invalid slice value for key '" + key + "': missing slice close ']'")
	}

	str = str[:len(str)-1]

	if err := util.SplitCommaSeparated(str, each); err != nil {
		panic("env: invalid elem in slice value for key '" + key + "': " + err.Error())
	}

	return true
}
