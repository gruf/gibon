package envflag

import (
	flags "flag"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-env"
)

type EnvFlag struct {
	Env   string
	Flag  string
	Usage string
}

func BoolVar(ptr *bool, flag EnvFlag, value bool) {
	value = (value || env.GetBool(flag.Env))
	flags.BoolVar(ptr, flag.Flag, value, flag.Usage)
}

func Bool(flag EnvFlag, value bool) *bool {
	ptr := new(bool)
	BoolVar(ptr, flag, value)
	return ptr
}

func IntVar(ptr *int, flag EnvFlag, value int) {
	if v := env.GetInt(flag.Env); v != 0 {
		value = v
	}
	flags.IntVar(ptr, flag.Flag, value, flag.Usage)
}

func Int(flag EnvFlag, value int) *int {
	ptr := new(int)
	IntVar(ptr, flag, value)
	return ptr
}

func Int64Var(ptr *int64, flag EnvFlag, value int64) {
	if v := env.GetInt64(flag.Env); v != 0 {
		value = v
	}
	flags.Int64Var(ptr, flag.Flag, value, flag.Usage)
}

func Int64(flag EnvFlag, value int64) *int64 {
	ptr := new(int64)
	Int64Var(ptr, flag, value)
	return ptr
}

func UintVar(ptr *uint, flag EnvFlag, value uint) {
	if v := env.GetUint(flag.Env); v != 0 {
		value = v
	}
	flags.UintVar(ptr, flag.Flag, value, flag.Usage)
}

func Uint(flag EnvFlag, value uint) *uint {
	ptr := new(uint)
	UintVar(ptr, flag, value)
	return ptr
}

func Uint64Var(ptr *uint64, flag EnvFlag, value uint64) {
	if v := env.GetUint64(flag.Env); v != 0 {
		value = v
	}
	flags.Uint64Var(ptr, flag.Flag, value, flag.Usage)
}

func Uint64(flag EnvFlag, value uint64) *uint64 {
	ptr := new(uint64)
	Uint64Var(ptr, flag, value)
	return ptr
}

func FloatVar(ptr *float64, flag EnvFlag, value float64) {
	if v := env.GetFloat(flag.Env); v != 0 {
		value = v
	}
	flags.Float64Var(ptr, flag.Flag, value, flag.Usage)
}

func Float(flag EnvFlag, value float64) *float64 {
	ptr := new(float64)
	FloatVar(ptr, flag, value)
	return ptr
}

func DurationVar(ptr *time.Duration, flag EnvFlag, value time.Duration) {
	if v := env.GetDuration(flag.Env); v != 0 {
		value = v
	}
	flags.DurationVar(ptr, flag.Flag, value, flag.Usage)
}

func Duration(flag EnvFlag, value time.Duration) *time.Duration {
	ptr := new(time.Duration)
	DurationVar(ptr, flag, value)
	return ptr
}

func TimeVar(ptr *time.Time, flag EnvFlag, value time.Time) {
	if v := env.GetTime(flag.Env); !v.IsZero() {
		value = v
	}
	*ptr = value
	flags.Var((*timeVar)(ptr), flag.Flag, flag.Usage)
}

func Time(flag EnvFlag, value time.Time) *time.Time {
	ptr := new(time.Time)
	TimeVar(ptr, flag, value)
	return ptr
}

func SizeVar(ptr *bytesize.Size, flag EnvFlag, value bytesize.Size) {
	if v := env.GetSize(flag.Env); v != 0 {
		value = v
	}
	*ptr = value
	flags.Var((*sizeVar)(ptr), flag.Flag, flag.Usage)
}

func Size(flag EnvFlag, value bytesize.Size) *bytesize.Size {
	ptr := new(bytesize.Size)
	SizeVar(ptr, flag, value)
	return ptr
}

func StringVar(ptr *string, flag EnvFlag, value string) {
	if v := env.Get(flag.Env); v != "" {
		value = v
	}
	flags.StringVar(ptr, flag.Flag, value, flag.Usage)
}

func String(flag EnvFlag, value string) *string {
	ptr := new(string)
	StringVar(ptr, flag, value)
	return ptr
}

func BoolArrayVar(ptr *[]bool, flag EnvFlag, value []bool) {
	if v := env.GetBoolArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*boolArray)(ptr), flag.Flag, flag.Usage)
}

func BoolArray(flag EnvFlag, value []bool) *[]bool {
	ptr := new([]bool)
	BoolArrayVar(ptr, flag, value)
	return ptr
}

func IntArrayVar(ptr *[]int, flag EnvFlag, value []int) {
	if v := env.GetIntArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*intArray)(ptr), flag.Flag, flag.Usage)
}

func IntArray(flag EnvFlag, value []int) *[]int {
	ptr := new([]int)
	IntArrayVar(ptr, flag, value)
	return ptr
}

func Int64ArrayVar(ptr *[]int64, flag EnvFlag, value []int64) {
	if v := env.GetInt64Array(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*int64Array)(ptr), flag.Flag, flag.Usage)
}

func Int64Array(flag EnvFlag, value []int64) *[]int64 {
	ptr := new([]int64)
	Int64ArrayVar(ptr, flag, value)
	return ptr
}

func UintArrayVar(ptr *[]uint, flag EnvFlag, value []uint) {
	if v := env.GetUintArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*uintArray)(ptr), flag.Flag, flag.Usage)
}

func UintArray(flag EnvFlag, value []uint) *[]uint {
	ptr := new([]uint)
	UintArrayVar(ptr, flag, value)
	return ptr
}

func Uint64ArrayVar(ptr *[]uint64, flag EnvFlag, value []uint64) {
	if v := env.GetUint64Array(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*uint64Array)(ptr), flag.Flag, flag.Usage)
}

func Uint64Array(flag EnvFlag, value []uint64) *[]uint64 {
	ptr := new([]uint64)
	Uint64ArrayVar(ptr, flag, value)
	return ptr
}

func FloatArrayVar(ptr *[]float64, flag EnvFlag, value []float64) {
	if v := env.GetFloatArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*floatArray)(ptr), flag.Flag, flag.Usage)
}

func FloatArray(flag EnvFlag, value []float64) *[]float64 {
	ptr := new([]float64)
	FloatArrayVar(ptr, flag, value)
	return ptr
}

func DurationArrayVar(ptr *[]time.Duration, flag EnvFlag, value []time.Duration) {
	if v := env.GetDurationArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*durationArray)(ptr), flag.Flag, flag.Usage)
}

func DurationArray(flag EnvFlag, value []time.Duration) *[]time.Duration {
	ptr := new([]time.Duration)
	DurationArrayVar(ptr, flag, value)
	return ptr
}

func TimeArrayVar(ptr *[]time.Time, flag EnvFlag, value []time.Time) {
	if v := env.GetTimeArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*timeArray)(ptr), flag.Flag, flag.Usage)
}

func TimeArray(flag EnvFlag, value []time.Time) *[]time.Time {
	ptr := new([]time.Time)
	TimeArrayVar(ptr, flag, value)
	return ptr
}

func SizeArrayVar(ptr *[]bytesize.Size, flag EnvFlag, value []bytesize.Size) {
	if v := env.GetSizeArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*sizeArray)(ptr), flag.Flag, flag.Usage)
}

func SizeArray(flag EnvFlag, value []bytesize.Size) *[]bytesize.Size {
	ptr := new([]bytesize.Size)
	SizeArrayVar(ptr, flag, value)
	return ptr
}

func StringArrayVar(ptr *[]string, flag EnvFlag, value []string) {
	if v := env.GetArray(flag.Env); v != nil {
		value = v
	}
	*ptr = value
	flags.Var((*stringArray)(ptr), flag.Flag, flag.Usage)
}

func StringArray(flag EnvFlag, value []string) *[]string {
	ptr := new([]string)
	StringArrayVar(ptr, flag, value)
	return ptr
}
