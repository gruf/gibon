package envflag

import (
	flags "flag"

	"codeberg.org/gruf/go-bytes"
)

func init() {
	// Replace default usage with our own.
	flags.CommandLine.Usage = func() {
		usage(flags.CommandLine)
	}
}

// Args is a call-through to flag.Args().
func Args() []string { return flags.Args() }

// CommandLine returns the flag global flag.CommandLine.
func CommandLine() *flags.FlagSet { return flags.CommandLine }

// Parse is a call-through to flag.Parse().
func Parse() { flags.Parse() }

// Parsed is a call-through to flag.Parsed().
func Parsed() bool { return flags.Parsed() }

// Defaults returns the formatted application flag argument defaults.
func Defaults() string {
	buf := bytes.Buffer{}
	defaults(CommandLine(), &buf)
	return buf.StringPtr()
}

// PrintDefaults prints formatted application flag argument defaults.
func PrintDefaults() {
	buf := bytes.Buffer{}
	defaults(flags.CommandLine, &buf)
	flags.CommandLine.Output().Write(buf.B)
}

// Set is a call-through to flag.Set().
func Set(name string, value string) error { return flags.Set(name, value) }
