package envflag

import (
	"strconv"
	"strings"
	"time"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-env/util"
)

type timeVar time.Time

func (v *timeVar) Set(value string) error {
	t, err := time.Parse(time.RFC1123, value)
	if err == nil {
		*v = timeVar(t)
	}
	return err
}

func (v timeVar) String() string {
	return (time.Time)(v).Format(time.RFC1123)
}

type sizeVar bytesize.Size

func (v *sizeVar) Set(value string) error {
	s, err := bytesize.ParseSize(value)
	if err == nil {
		*v = sizeVar(s)
	}
	return err
}

func (v sizeVar) String() string {
	return (bytesize.Size)(v).StringIEC()
}

type boolArray []bool

func (a *boolArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.ParseBool(str)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a boolArray) String() string {
	buf := bytes.Buffer{}

	for _, b := range a {
		buf.B = strconv.AppendBool(buf.B, b)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type intArray []int

func (a *intArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.Atoi(str)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a intArray) String() string {
	buf := bytes.Buffer{}

	for _, i := range a {
		buf.B = strconv.AppendInt(buf.B, int64(i), 10)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type int64Array []int64

func (a *int64Array) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.ParseInt(str, 10, 64)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a int64Array) String() string {
	buf := bytes.Buffer{}

	for _, i := range a {
		buf.B = strconv.AppendInt(buf.B, i, 10)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type uintArray []uint

func (a *uintArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.ParseUint(str, 10, 64)
		if err == nil {
			*a = append(*a, uint(val))
		}
		return err
	})
}

func (a uintArray) String() string {
	buf := bytes.Buffer{}

	for _, u := range a {
		buf.B = strconv.AppendUint(buf.B, uint64(u), 10)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type uint64Array []uint64

func (a *uint64Array) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.ParseUint(str, 10, 64)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a uint64Array) String() string {
	buf := bytes.Buffer{}

	for _, u := range a {
		buf.B = strconv.AppendUint(buf.B, u, 10)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type floatArray []float64

func (a *floatArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := strconv.ParseFloat(str, 64)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a floatArray) String() string {
	buf := bytes.Buffer{}

	for _, f := range a {
		buf.B = strconv.AppendFloat(buf.B, f, 'f', -1, 64)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type durationArray []time.Duration

func (a *durationArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := time.ParseDuration(str)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a durationArray) String() string {
	buf := bytes.Buffer{}

	for _, d := range a {
		buf.WriteString(d.String())
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type timeArray []time.Time

func (a *timeArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := time.Parse(time.RFC1123, str)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a timeArray) String() string {
	buf := bytes.Buffer{}

	for _, t := range a {
		buf.WriteByte('"')
		buf.B = t.AppendFormat(buf.B, time.RFC1123)
		buf.WriteString("\",")
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type sizeArray []bytesize.Size

func (a *sizeArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		val, err := bytesize.ParseSize(str)
		if err == nil {
			*a = append(*a, val)
		}
		return err
	})
}

func (a sizeArray) String() string {
	buf := bytes.Buffer{}

	for _, s := range a {
		buf.B = s.AppendFormatIEC(buf.B)
		buf.WriteByte(',')
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}

type stringArray []string

func (a *stringArray) Set(value string) error {
	*a = (*a)[0:0]
	return util.SplitCommaSeparated(value, func(str string) error {
		*a = append(*a, str)
		return nil
	})
}

func (a stringArray) String() string {
	buf := bytes.Buffer{}

	for _, str := range a {
		buf.WriteByte('"')
		buf.WriteString(strings.ReplaceAll(str, "\"", "\\\""))
		buf.WriteString("\",")
	}

	if len(a) > 0 {
		buf.Truncate(1)
	}

	return buf.StringPtr()
}
