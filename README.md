# Gibon

A CURL-friendly pastebin service written in GoLang!

Storage is handled by chunking supplied data to place by hash in a disk-backed key-value store, i.e. homegrown poor-man's IPFS. The data is streamed both to and from the database, and the architecture is optimized for concurrency. This method should mean that over-time you save disk-space by reusing blocks of similar data between files!

HTTP multiplexing handled by: [httprouter](https://github.com/julienschmidt/httprouter)

Storage handled by my own key-value store: [go-store](https://git.iim.gay/grufwub/go-store)

And logging with my own package: [go-logger](https://git.iim.gay/grufwub/go-logger)

# Usage (flag, environment variable)

- `-block-size`, `$BLOCK_SIZE` = Store block size

- `-hostname`, `$HOSTNAME` = Hostname used in generating root help message

- `-http2`, `$HTTP2` = Enable HTTP2 support

- `-listen`, `$LISTEN` = Server listen address

- `-max-file-size`, `$MAX_FILE_SIZE` = Maximum file size

- `-read-buffer-size`, `$READ_BUFFER_SIZE` = Store read buffer size

- `-store-path`, `$STORE_PATH` = Gibon store path

- `-tls-cert`, `$TLS_CERT` = Server TLS cert file

- `-tls-key`, `$TLS_KEY` = Server TLS key file

- `-write-buffer-size`, `$WRITE_BUFFER_SIZE` = Store write buffer size

# Building

To build locally: `scripts/build_local.sh`

To build docker image: `docker build .`

Sample env file: `scripts/sample.env`

To build and run docker image: `scripts/docker_run.sh <host_root> <env_file>`
