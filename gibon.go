package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-env/envflag"
	"codeberg.org/gruf/go-format"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v2/httplog"
	"codeberg.org/gruf/go-logger/v2/log"
	"codeberg.org/gruf/go-maxbytes"
	"codeberg.org/gruf/go-middleware"
	"codeberg.org/gruf/go-mimetypes"
	"codeberg.org/gruf/go-pools"
	kvstore "codeberg.org/gruf/go-store/kv"
	"codeberg.org/gruf/go-store/storage"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/net/http2"
)

var (
	// Compiler set version info.
	Commit  string
	Version string

	// Customizable (by hostname) help page string.
	rootHelpStr = `Gibon -- a file bin service!

Usage:
$ curl https://%s/file/<FILENAME> --data 'paste text goes here'
--> '/file/<FILENAME>'

$ curl https://%s/file/<FILENAME>
--> 'paste text goes here'

Note:
You can suggest a content-type for your file by passing a
filename with an extension. Failing that, Gibon will attempt
to automatically determine a content-type for you
`

	// robots text page.
	robotsTxtStr = `User-agent: *
Disallow: /
`

	// maximum request body size.
	maxFileSize bytesize.Size

	// buffered reader pool.
	bufReaderPool pools.BufioReaderPool

	// store is the global block store.
	store *kvstore.KVStore

	// global HTTP middleware.
	httputil = middleware.New((*httplog.Logger)(log.Logger))
)

func s2b(s string) []byte {
	return bytes.StringToBytes(s)
}

func plainTextHandler(text string) httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		rw.WriteHeader(http.StatusOK)
		rw.Header().Set("Content-Type", "text/plain")
		rw.Write(s2b(text)) //nolint
	}
}

func getFileHandler(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	name := p[0].Value

	// Attempt to fetch paste from store
	rc, err := store.GetStream(name)
	if err != nil {
		http.Error(rw, "File Not Found", 404)
		return
	}
	defer rc.Close()

	// Wrap in a buffered reader
	br := bufReaderPool.Get(rc)
	defer bufReaderPool.Put(br)

	// Attempt to get mimetype for filename
	contentType, ok := mimetypes.GetForFilename(name)
	if !ok {
		// Peek first 512 bytes
		b, err := br.Peek(512)
		if err != nil && err != io.EOF {
			httputil.HTTPError(rw, r, "Internal Server Error", 500, err)
			return
		}

		// Determine content-type from bytes!
		contentType = http.DetectContentType(b)
	}

	// Write content-type header
	rw.Header().Set("Content-Type", contentType)

	// Write response to client
	if _, err := br.WriteTo(rw); err != nil {
		httputil.HTTPError(rw, r, "Internal Server Error", 500, err)
		return
	}
}

func putFileHandler(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	name := p[0].Value

	// Put the body in the database!
	err := store.PutStream(name, maxbytes.NewReader(r.Body, int64(maxFileSize)))
	switch err {
	// All good
	case nil:

	// Catch already-exists
	case storage.ErrAlreadyExists:
		http.Error(rw, "Key Already Exists", 400)
		return

	// Catch bad-key errors
	case storage.ErrInvalidKey:
		http.Error(rw, "Bad Key", 400)
		return

	// Mask all else with internal error
	default:
		httputil.HTTPError(rw, r, "Internal Server Error", 500, err)
		return
	}

	// Write the store path in response
	rw.Header().Set("Content-Type", "text/plain")
	rw.Write(s2b("/file/" + name + "\n"))
}

func main() {
	var (
		err       error
		hostname  string
		listen    string
		useHTTP2  bool
		storePath string
		tlsCert   string
		tlsKey    string
		blockSize bytesize.Size
		writeBuf  bytesize.Size
		readBuf   bytesize.Size
		version   bool
	)

	// Set flags and parse!
	envflag.StringVar(&hostname, envflag.EnvFlag{
		Env:   "HOSTNAME",
		Flag:  "hostname",
		Usage: "Hostname used in generating root help message",
	}, "")
	envflag.StringVar(&listen, envflag.EnvFlag{
		Env:   "LISTEN",
		Flag:  "listen",
		Usage: "Server listen address",
	}, "127.0.0.1:8080")
	envflag.BoolVar(&useHTTP2, envflag.EnvFlag{
		Env:   "HTTP2",
		Flag:  "http2",
		Usage: "Enable http2 support",
	}, true)
	envflag.StringVar(&storePath, envflag.EnvFlag{
		Env:   "STORE_PATH",
		Flag:  "store-path",
		Usage: "Gibon store path",
	}, "./gibon.store")
	envflag.StringVar(&tlsCert, envflag.EnvFlag{
		Env:   "TLS_CERT",
		Flag:  "tls-cert",
		Usage: "Server TLS cert file",
	}, "")
	envflag.StringVar(&tlsKey, envflag.EnvFlag{
		Env:   "TLS_KEY",
		Flag:  "tls-key",
		Usage: "Server TLS key file",
	}, "")
	envflag.SizeVar(&maxFileSize, envflag.EnvFlag{
		Env:   "MAX_FILE_SIZE",
		Flag:  "max-file-size",
		Usage: "Maximum file size",
	}, 1024*1024*5)
	envflag.SizeVar(&blockSize, envflag.EnvFlag{
		Env:   "BLOCK_SIZE",
		Flag:  "block-size",
		Usage: "Store block size",
	}, 1024*16)
	envflag.SizeVar(&writeBuf, envflag.EnvFlag{
		Env:   "WRITE_BUFFER_SIZE",
		Flag:  "write-buffer-size",
		Usage: "Store write buffer size",
	}, 1024*8)
	envflag.SizeVar(&readBuf, envflag.EnvFlag{
		Env:   "READ_BUFFER_SIZE",
		Flag:  "read-buffer-size",
		Usage: "Store read buffer size",
	}, 1024*8)
	flag.BoolVar(
		&version,
		"version",
		false,
		"Print version string",
	)
	envflag.Parse()

	if version {
		// Print version string and exit application
		println("gibon (" + Version + "-" + Commit + ")")
		syscall.Exit(0)
	}

	// Check for store path
	if storePath == "" {
		log.Fatal("No store path supplied!")
	}

	// Open database at supplied path
	log.Infof("Opening store: %s", storePath)
	store, err = kvstore.OpenBlock(storePath, &storage.BlockConfig{
		BlockSize:    int(blockSize),
		WriteBufSize: int(writeBuf),
		Compression:  storage.NoCompression(),
	})
	if err != nil {
		log.Fatal(err)
	}

	// Set default hostname
	if hostname == "" {
		hostname = listen
	}

	// Construct the HTTP root site help string
	buf := format.Buffer{}
	fmt.Fprintf(&buf, rootHelpStr, hostname, hostname)
	rootHelpStr = buf.String()

	// Setup HTTP router
	router := httprouter.New()
	router.NotFound = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		http.Error(rw, "Not Found", 404)
	})
	router.GET("/", plainTextHandler(rootHelpStr))
	router.GET("/robots.txt", plainTextHandler(robotsTxtStr))
	router.POST("/file/:filename", putFileHandler)
	router.GET("/file/:filename", getFileHandler)

	// Prepare handler with logging
	router.PanicHandler = nil
	handler := (http.Handler)(router)
	handler = httputil.Recovery(handler)
	handler = httputil.Logger(handler)
	handler = httputil.RequestID(handler)

	// Prepare read buffered reader pool
	bufReaderPool = pools.NewBufioReaderPool(int(readBuf))

	// Create the HTTP server
	srv := http.Server{
		Addr:    listen,
		Handler: handler,
	}

	if useHTTP2 {
		// Enable HTTP2 server support
		err := http2.ConfigureServer(&srv, nil)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Log server configuration
	log.InfoKVs(
		kv.Field{K: "http2", V: useHTTP2},
		kv.Field{K: "tls-cert", V: tlsCert},
		kv.Field{K: "tls-key", V: tlsKey},
		kv.Field{K: "max-file-size", V: maxFileSize},
		kv.Field{K: "block-size", V: blockSize},
		kv.Field{K: "write-buffer-size", V: writeBuf},
		kv.Field{K: "read-buffer-size", V: readBuf},
	)

	// Listen for OS signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		// Wait on signal received
		if sig, ok := <-sigs; ok {
			log.Infof(`received signal %q, shutting down...`, sig)
		}

		// Shutdown srv
		_ = srv.Close()
	}()

	switch {
	// TLS cert and key file provided, use HTTPS
	case tlsCert != "" && tlsKey != "":
		log.Info("listening on https://" + listen)
		err = srv.ListenAndServeTLS(tlsCert, tlsKey)

	// Use regular HTTP
	default:
		log.Info("listening on: http://" + listen)
		err = srv.ListenAndServe()
	}

	// Catch errors
	if err != nil && err != http.ErrServerClosed {
		log.Fatalf("error listening on HTTP server: %v", err)
	}
}
